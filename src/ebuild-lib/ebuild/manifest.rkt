;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (prefix-in port: (only-in racket/port port->lines))
         (prefix-in string: (only-in racket/string string-join))
         threading)

(provide (all-defined-out))


#|
What do Manifest files hold?

DIST monocalendar-source-0.7.2.tar.gz 178249 BLAKE2B ed1 SHA512 879
^    ^ file (mostly a tarball)        ^ size ^       ^ hash
| type: dist/ebuild/misc                     | hash type
|#


(struct checksum
  ([type : String] [hash : String])
  #:mutable
  #:transparent
  #:type-name Checksum)

(define (list->checksums [lst : (Listof String)]) : (Listof Checksum)
  (unless (= (remainder (length lst) 2) 0)
    (error 'list->checksums "uneven list length, given ~v" lst))
  (let loop ([lst : (Listof String) lst]
             [checksums : (Listof Checksum) '()])
    (cond
      [(null? lst) checksums]
      [else
       (loop (list-tail lst 2)
             (append checksums
                     (list (checksum (list-ref lst 0) (list-ref lst 1)))))])))

(define (checksum->string [cs : Checksum]) : String
  (string-append (checksum-type cs) " " (checksum-hash cs)))


(define-type Dist-Type
  (U 'dist 'ebuild 'misc))

(struct dist
  ([type      : Dist-Type]
   [file      : String]
   [size      : Integer]
   [checksums : (Listof Checksum)])
  #:mutable
  #:transparent
  #:type-name Dist)

(define (string->dist-type [str : String]) : Dist-Type
  (case str
    [("DIST")   'dist]
    [("EBUILD") 'ebuild]
    [("MISC")   'misc]
    [else
     (error 'string->dis "wrong type, given ~v" str)]))

(define (dist-type->string [dist-type : Dist-Type]) : String
  (case dist-type
    [(dist)   "DIST"]
    [(ebuild) "EBUILD"]
    [(misc)   "MISC"]))

(define (string->dist [str : String]) : Dist
  (let ([lst (regexp-split " " str)])
    (dist (string->dist-type (list-ref lst 0))
          (list-ref lst 1)
          (let ([num (string->number (list-ref lst 2))])
            (cond
              [(and (integer? num) (not (inexact? num))) num]
              [else
               (error 'string->dist "could not parse ~v as integer" num)]))
          (list->checksums (list-tail lst 3)))))

(define (dist->string [dst : Dist]) : String
  (format "~a ~a ~a ~a"
          (dist-type->string (dist-type dst))
          (dist-file dst)
          (dist-size dst)
          (~>> dst
               dist-checksums
               (map checksum->string)
               string:string-join)))


(struct manifest
  ([dists : (Listof Dist)])
  #:mutable
  #:transparent
  #:type-name Manifest)

(define (read-manifest [inp : Input-Port]) : Manifest
  (~>> inp
       port:port->lines
       (map string->dist)
       manifest))

(define (read-manifest-file [pth : Path-String]) : Manifest
  (~>> pth
       open-input-file
       read-manifest))

(define (write-manifest [mnfst : Manifest]
                        [out : Output-Port (current-output-port)]) : Void
  (for ([dst (manifest-dists mnfst)])
    (fprintf out "~a~%" (dist->string dst))))

(define manifest-save-exists-action
  : (Parameterof (U 'append 'can-update 'error 'must-truncate 'replace
                    'truncate 'truncate/replace 'update))
  (make-parameter 'replace))

(define (save-manifest [mnfst : Manifest] [pth : Path-String]) : Void
  (with-output-to-file pth #:exists (manifest-save-exists-action)
    (lambda _ (write-manifest mnfst))))
