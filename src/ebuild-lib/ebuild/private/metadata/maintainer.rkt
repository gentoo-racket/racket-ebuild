;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require "element.rkt")

(provide (all-defined-out))


(struct maintainer
  ([type         : (U 'person 'project 'unknown)]
   [proxied      : (Option (U 'yes 'no 'proxy))]
   [email        : String]
   [name         : (Option String)]
   [description  : (Option String)])
  #:mutable
  #:transparent
  #:type-name Maintainer)


(define (maintainer->xexpr [maint : Maintainer]) : (Listof Any)
  `(maintainer ((type ,(symbol->string (maintainer-type maint)))
                ,@(full->element (maintainer-proxied maint) 'proxied))
               (email ,(maintainer-email maint))
               ,@(full->element (maintainer-name        maint) 'name)
               ,@(full->element (maintainer-description maint) 'description)))
