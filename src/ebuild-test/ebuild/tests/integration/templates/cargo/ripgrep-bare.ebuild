# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	"

inherit cargo

DESCRIPTION="Package for ripgrep test"
HOMEPAGE="https://github.com/BurntSushi/ripgrep"
SRC_URI="$(cargo_crate_uris ${CRATES})"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
