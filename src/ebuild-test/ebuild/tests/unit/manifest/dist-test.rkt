;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit ebuild/manifest)

  (let ([dist-types-hash (hash 'dist "DIST" 'ebuild "EBUILD" 'misc "MISC")])
    (for ([(sym str) (in-hash dist-types-hash)])
      (test-equal? (format "~a symbol type" sym)
                   (dist-type->string sym) str)
      (test-equal? (format "~a type symbol" str)
                   (string->dist-type str) sym)))

  (let ([monocalendar-string
         "DIST monocalendar-source-0.7.2.tar.gz 178249 BLAKE2B ed1 SHA512 879"]
        [monocalendar-dist
         (dist 'dist "monocalendar-source-0.7.2.tar.gz" 178249
               (list (checksum "BLAKE2B" "ed1") (checksum "SHA512" "879")))])
    (test-equal? "String to Dist struct: monocalendar"
                 (string->dist monocalendar-string) monocalendar-dist)
    (test-equal? "Dist struct to string: monocalendar"
                 (dist->string monocalendar-dist) monocalendar-string)))
