;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           ebuild/transformers/cflag
           (only-in ebuild/ebuild cflag))

  (test-equal? "Singular, flat"
               (string->cflag "z") '("z"))
  (test-equal? "Double, flat"
               (string->cflag "z x") '("z" "x"))

  (test-equal? "Depth 1"
               (string->cflag "a? ( a )")
               (list (cflag "a?" (list "a"))))
  (test-equal? "Depth 2"
               (string->cflag "a? ( b? ( b ) )")
               (list (cflag "a?" (list (cflag "b?" (list "b"))))))
  (test-equal? "Depth 3"
               (string->cflag "a? ( b? ( c? ( c ) ) )")
               (list (cflag "a?" (list (cflag "b?" (list (cflag "c?" '("c"))))))))
  (test-equal? "Depth 4"
               (string->cflag "a? ( b? ( c? ( d? ( d ) ) ) )")
               (list (cflag "a?" (list (cflag "b?" (list (cflag "c?" (list (cflag "d?" '("d"))))))))))

  (test-equal? "Double, depth 1"
               (string->cflag "z? ( z ) x? ( x )")
               (list (cflag "z?" '("z")) (cflag "x?" '("x"))))
  (test-equal? "Double, depth 2"
               (string->cflag "z? ( zz? ( zz ) ) x? ( xx? ( xx ) )")
               (list (cflag "z?" (list (cflag "zz?" '("zz"))))
                     (cflag "x?" (list (cflag "xx?" '("xx"))))))

  (test-equal? "Real case: sci-mathematics/why3-1.4.0-r2"
               (string->cflag
                "!sci-mathematics/why3-for-spark
                >=dev-lang/ocaml-4.05.0:=[ocamlopt?]
                >=dev-ml/menhir-20151112:=
                dev-ml/num:=
                coq? ( >=sci-mathematics/coq-8.6 )
                emacs? ( app-editors/emacs:* )
                gtk? ( dev-ml/lablgtk:=[sourceview,ocamlopt?] )
                re? ( dev-ml/re:= dev-ml/seq:= )
                sexp? (
                    dev-ml/ppx_deriving:=[ocamlopt?]
                    dev-ml/ppx_sexp_conv:=[ocamlopt?]
                    dev-ml/sexplib:=[ocamlopt?]
                )
                zarith? ( dev-ml/zarith:= )
                zip? ( dev-ml/camlzip:= )")
               (list
                "!sci-mathematics/why3-for-spark"
                ">=dev-lang/ocaml-4.05.0:=[ocamlopt?]"
                ">=dev-ml/menhir-20151112:="
                "dev-ml/num:="
                (cflag "coq?" '(">=sci-mathematics/coq-8.6"))
                (cflag "emacs?" '("app-editors/emacs:*"))
                (cflag "gtk?" '("dev-ml/lablgtk:=[sourceview,ocamlopt?]"))
                (cflag "re?" '("dev-ml/re:=" "dev-ml/seq:="))
                (cflag "sexp?" '("dev-ml/ppx_deriving:=[ocamlopt?]"
                                 "dev-ml/ppx_sexp_conv:=[ocamlopt?]"
                                 "dev-ml/sexplib:=[ocamlopt?]"))
                (cflag "zarith?" '("dev-ml/zarith:="))
                (cflag "zip?" '("dev-ml/camlzip:="))))

  (test-equal? "Real case: sys-apps/portage-9999"
               (string->cflag
                "acct-user/portage
                app-arch/zstd
                >=app-arch/tar-1.27
                dev-lang/python-exec:2
                >=sys-apps/findutils-4.4
                !build? (
                    >=sys-apps/sed-4.0.5
                    >=app-shells/bash-5.0:0[readline]
                    >=app-admin/eselect-1.2
                    rsync-verify? (
                        >=app-portage/gemato-14.5[${PYTHON_USEDEP}]
                        >=sec-keys/openpgp-keys-gentoo-release-20180706
                        >=app-crypt/gnupg-2.2.4-r2[ssl(-)]
                    )
                )
                elibc_glibc? ( >=sys-apps/sandbox-2.2 )
                elibc_musl? ( >=sys-apps/sandbox-2.2 )
                kernel_linux? ( sys-apps/util-linux )
                >=app-misc/pax-utils-0.1.17
                selinux? ( >=sys-libs/libselinux-2.0.94[python,${PYTHON_USEDEP}] )
                xattr? ( kernel_linux? (
                    >=sys-apps/install-xattr-0.3
                ) )
                !<app-admin/logrotate-3.8.0
                !<app-portage/gentoolkit-0.4.6
                !<app-portage/repoman-2.3.10
                !~app-portage/repoman-3.0.0")
               (list
                "acct-user/portage"
                "app-arch/zstd"
                ">=app-arch/tar-1.27"
                "dev-lang/python-exec:2"
                ">=sys-apps/findutils-4.4"
                (cflag
                 "!build?"
                 (list
                  ">=sys-apps/sed-4.0.5"
                  ">=app-shells/bash-5.0:0[readline]"
                  ">=app-admin/eselect-1.2"
                  (cflag "rsync-verify?"
                         '(">=app-portage/gemato-14.5[${PYTHON_USEDEP}]"
                           ">=sec-keys/openpgp-keys-gentoo-release-20180706"
                           ">=app-crypt/gnupg-2.2.4-r2[ssl(-)]"))))
                (cflag "elibc_glibc?" '(">=sys-apps/sandbox-2.2"))
                (cflag "elibc_musl?" '(">=sys-apps/sandbox-2.2"))
                (cflag "kernel_linux?" '("sys-apps/util-linux"))
                ">=app-misc/pax-utils-0.1.17"
                (cflag "selinux?"
                       '(">=sys-libs/libselinux-2.0.94[python,${PYTHON_USEDEP}]"))
                (cflag "xattr?" (list (cflag "kernel_linux?"
                                             '(">=sys-apps/install-xattr-0.3"))))
                "!<app-admin/logrotate-3.8.0"
                "!<app-portage/gentoolkit-0.4.6"
                "!<app-portage/repoman-2.3.10"
                "!~app-portage/repoman-3.0.0")))
