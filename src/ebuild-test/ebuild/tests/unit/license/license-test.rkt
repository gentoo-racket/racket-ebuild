;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit ebuild/license)

  ;; Licene naming conventions

  (test-equal? "BSD"       (spdx-license->ebuild-license 'BSD-3-Clause)      'BSD)
  (test-equal? "BSD-1"     (spdx-license->ebuild-license 'BSD-1-Clause)      'BSD-1)
  (test-equal? "BSD-2"     (spdx-license->ebuild-license 'BSD-2-Clause)      'BSD-2)

  (test-equal? "AGPL-3"    (spdx-license->ebuild-license 'AGPL-3.0-only)     'AGPL-3)
  (test-equal? "AGPL-3+"   (spdx-license->ebuild-license 'AGPL-3.0-or-later) 'AGPL-3+)

  (test-equal? "GPL-2"     (spdx-license->ebuild-license 'GPL-2.0-only)      'GPL-2)
  (test-equal? "GPL-2+"    (spdx-license->ebuild-license 'GPL-2.0-or-later)  'GPL-2+)
  (test-equal? "GPL-3"     (spdx-license->ebuild-license 'GPL-3.0-only)      'GPL-3)
  (test-equal? "GPL-3+"    (spdx-license->ebuild-license 'GPL-3.0-or-later)  'GPL-3+)

  (test-equal? "LGPL-2"    (spdx-license->ebuild-license 'LGPL-2.0-only)     'LGPL-2)
  (test-equal? "LGPL-2+"   (spdx-license->ebuild-license 'LGPL-2.0-or-later) 'LGPL-2+)
  (test-equal? "LGPL-2.1"  (spdx-license->ebuild-license 'LGPL-2.1-only)     'LGPL-2.1)
  (test-equal? "LGPL-2.1+" (spdx-license->ebuild-license 'LGPL-2.1-or-later) 'LGPL-2.1+)
  (test-equal? "LGPL-3"    (spdx-license->ebuild-license 'LGPL-3.0-only)     'LGPL-3)
  (test-equal? "LGPL-3+"   (spdx-license->ebuild-license 'LGPL-3.0-or-later) 'LGPL-3+)

  ;; License expressions

  (test-equal? "GPL-2+"     (spdx-string->ebuild-string "GPL-2.0-or-later") "GPL-2+")
  (test-equal? "Apache-2.0" (spdx-string->ebuild-string "Apache-2.0") "Apache-2.0")
  (test-equal? "MIT"        (spdx-string->ebuild-string "MIT") "MIT")

  (test-equal? "Apache-2.0 and MIT"
               (spdx-string->ebuild-string "(Apache-2.0 AND MIT)")
               "Apache-2.0 MIT")
  (test-equal? "Apache-2.0 or MIT"
               (spdx-string->ebuild-string "(Apache-2.0 OR MIT)")
               "|| ( Apache-2.0 MIT )")

  (test-equal? "GPL-2+ and Apache-2.0 and MIT"
               (spdx-string->ebuild-string "(GPL-2.0-or-later AND (Apache-2.0 AND MIT))")
               "GPL-2+ Apache-2.0 MIT")
  (test-equal? "GPL-2+ or Apache-2.0 or MIT"
               (spdx-string->ebuild-string "(GPL-2.0-or-later OR (Apache-2.0 OR MIT))")
               "|| ( GPL-2+ || ( Apache-2.0 MIT ) )")

  (test-equal? "GPL-2+ and Apache-2.0 or MIT"
               (spdx-string->ebuild-string "(GPL-2.0-or-later AND (Apache-2.0 OR MIT))")
               "GPL-2+ || ( Apache-2.0 MIT )")
  (test-equal? "GPL-2+ or Apache-2.0 and MIT"
               (spdx-string->ebuild-string "(GPL-2.0-or-later OR (Apache-2.0 AND MIT))")
               "|| ( GPL-2+ Apache-2.0 MIT )"))
