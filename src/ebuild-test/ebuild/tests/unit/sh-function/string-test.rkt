;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           ebuild/sh-function)

  (define myfun0
    (sh-function "src_prepare"
                 (make-script)))
  (define myfun1
    (sh-function "src_prepare"
                 (make-script "default")))
  (define myfun2
    (sh-function "src_prepare"
                 (make-script "unset PLTUSERHOME"
                              "xdg_environment_reset"
                              "default")))
  (define myfun3
    (sh-function "src_compile"
                 (make-script
                  "if use jit && ! use chez; then"
                  (make-script
                   "pushd ./bc || die"
                   "emake cgc-core"
                   "pax-mark m .libs/racketcgc"
                   ""
                   "pushd ./gc2 || die"
                   "emake all"
                   "popd || die"
                   ""
                   "pax-mark m .libs/racket3m"
                   "popd || die")
                  "fi"
                  ""
                  "default")))

  (define lfun
    (list myfun0 myfun1 myfun2 myfun3))

  (check-true (string? (sh-function->string myfun0)))
  (check-true (string? (unroll-sh-functions lfun))))
