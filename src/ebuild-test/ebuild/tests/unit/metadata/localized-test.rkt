;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require
   rackunit
   xml
   ebuild/metadata)

  ;; Proper forms

  (test-equal?
   "Proper localized argument without false fields"
   (localized->xexpr (localized 'en "asd") 'e) '(e ((lang "en")) "asd"))

  (test-equal?
   "Proper localized argument where \"lang\" is False"
   (localized->xexpr (localized #f "asd") 'e) '(e () "asd"))

  ;; Conversion

  (test-equal?
   "Metadata conversion of proper localized argument"
   (xexpr->xml (localized->xexpr (localized 'en "asd") 'e))
   (element 'racket 'racket 'e
            (list (attribute 'scheme 'scheme 'lang "en"))
            (list (pcdata 'racket 'racket "asd"))))

  ;; Errors

  (test-exn
   "Fail with empty string for \"lang\" field"
   exn:fail?
   (lambda () (localized "" "asd")))

  (test-exn
   "Fail with empty symbol for \"lang\" field"
   exn:fail?
   (lambda () (localized '|| "asd")))

  (test-exn
   "Fail with empty string for \"data\" field"
   exn:fail?
   (lambda () (localized 'en "")))
  )
