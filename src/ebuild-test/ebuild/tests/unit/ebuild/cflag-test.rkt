;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang ebuild


(module+ test
  (require rackunit)

  (test-exn "Fail on blank predicate" exn:fail? (lambda () (cflag "" '("a"))))

  (test-exn "Fail on predicate with a space"
            exn:fail?
            (lambda () (cflag "em acs?" '("a"))))

  (test-exn "Fail on empty content" exn:fail? (lambda () (cflag "X?" '())))

  (check-equal? (cflag->string (cflag "z?" '("z1"))) "z? ( z1 )")

  (check-equal? (cflag->string (cflag "z?" '("z1")) #:flat? #t) "z? ( z1 )")

  (check-equal? (cflag->string (cflag "z?" '("z1" "z2"))) "z? ( z1\n\tz2 )")

  (check-equal? (cflag->string (cflag "z?" '("z1" "z2")) #:flat? #t)
                "z? ( z1 z2 )"))
