;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang at-exp ebuild


(module+ test
  (require rackunit racket/format)

  (define ebuild-cyclone%
    (class ebuild%
      (super-new
       [EAPI 8]
       [DESCRIPTION "R7RS Scheme to C compiler"]
       [HOMEPAGE "http://justinethier.github.io/cyclone/"]
       [LICENSE "MIT"]
       [SLOT "0"]
       [RDEPEND '("dev-libs/concurrencykit")]
       [DEPEND '("${RDEPEND}")]
       [body (list
              @~a{
                  src_configure() {
                  @"\t"export CYC_GCC_OPT_FLAGS="${CFLAGS}"
                  @"\t"append-cflags -fPIC -Iinclude
                  @"\t"append-ldflags -L.
                  @"\t"tc-export AR CC RANLIB
                  }

                  src_test() {
                  @"\t"emake test LDFLAGS=""
                  }

                  src_compile() {
                  @"\t"local myopts=(
                  @"\t\t"PREFIX="${EPREFIX}/usr"
                  @"\t\t"CYC_GCC_OPT_FLAGS="${CYC_GCC_OPT_FLAGS}"
                  @"\t\t"AR="$(tc-getAR)"
                  @"\t\t"CC="$(tc-getCC)"
                  @"\t\t"RANLIB="$(tc-getRANLIB)"
                  @"\t")
                  @"\t"emake "${myopts[@"@"]}"
                  }

                  src_install() {
                  @"\t"einstalldocs
                  @"\t"emake PREFIX="/usr" DESTDIR="${D}" install
                  }
                  })])
      (ebuild-append! inherits this '("flag-o-matic" "toolchain-funcs"))))

  (define cyclone-ebuild-release
    (new
     ebuild-cyclone%
     [SRC_URI
      (list
       (src-uri
        #f
        "https://github.com/justinethier/${PN}-bootstrap/archive/refs/tags/v${PV}.tar.gz"
        "${P}.tar.gz"))]
     [S "${WORKDIR}/${PN}-bootstrap-${PV}"]
     [KEYWORDS '("~amd64")]))

  (define cyclone-ebuild-live
    (new ebuild-cyclone%
         [custom
          (list (lambda ()
                  (as-variable
                   "EGIT_REPO_URI"
                   "https://github.com/justinethier/${PN}-bootstrap.git")))]
         [inherits '("git-r3")]
         [KEYWORDS '()]))

  (define cyclone-metadata
    (new metadata%
         [maintainers
          (list (maintainer 'person #f "xgqt@riseup.net" "Maciej Barć" #f))]
         [upstream
          (upstream '()
                    #f
                    #f
                    #f
                    (list (remote-id 'github "justinethier/cyclone-bootstrap")
                          (remote-id 'github "justinethier/cyclone")))]))

  (define cyclone-package
    (new package%
         [CATEGORY "dev-scheme"]
         [PN "cyclone"]
         [ebuilds
          (hash (live-version)
                cyclone-ebuild-live
                (package-version "0.28.0" #f #f #f #f #f)
                cyclone-ebuild-release)]
         [metadata cyclone-metadata]))

  ;; Are we alive?
  (check-not-false (object? cyclone-package))
  (check-not-false (send cyclone-package show)))
