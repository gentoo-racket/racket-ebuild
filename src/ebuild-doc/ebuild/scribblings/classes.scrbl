;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket ebuild))


@title[#:tag "ebuild-classes"]{Ebuild - Classes}


@defmodule[ebuild]

@racket[ebuild] module reexports functions from the @racket[class] module
and modules included in this section, that is: @racket[ebuild/ebuild],
@racket[ebuild/metadata], @racket[ebuild/package]
and @racket[ebuild/repository]


@include-section{classes/ebuild.scrbl}
@include-section{classes/metadata.scrbl}
@include-section{classes/package.scrbl}
@include-section{classes/repository.scrbl}
