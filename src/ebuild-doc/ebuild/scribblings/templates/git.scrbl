;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/templates/git))


@(define git-eval
   (make-base-eval '(require racket/class ebuild/ebuild ebuild/templates/git)))


@title[#:tag "ebuild-templates-git"]{Git repositories}


@defmodule[ebuild/templates/git]


@defmixin[ebuild-git-mixin (ebuild%) ()]{}


@defclass[
 ebuild-git% ebuild% ()
 ]{
 Pre-made class extending @racket[ebuild%] for writing ebuilds using the
 @link["https://gitweb.gentoo.org/repo/gentoo.git/tree/eclass/git-r3.eclass"]{
  git-r3.eclass}.

 @defconstructor[
 (
  [EGIT_REPO_URI        string?]
  [EGIT_BRANCH          (or/c #f string?)  #f]
  [EGIT_CHECKOUT_DIR    (or/c #f string?)  #f]
  [EGIT_COMMIT          (or/c #f string?)  #f]
  [EGIT_COMMIT_DATE     (or/c #f string?)  #f]
  [EGIT_MIN_CLONE_TYPE  (or/c #f string?)  #f]
  [EGIT_MIRROR_URI      (or/c #f string?)  #f]
  [EGIT_SUBMODULES      (or/c #f (listof string?))  #f]
  [EVCS_OFFLINE         (or/c #f string?)  #f]
  )
 ]{
 }

 @examples[
 #:eval git-eval
 (define my-ebuild
   (new ebuild-git%
        [EGIT_REPO_URI "https://gitlab.com/asd/asd.git"]
        [EGIT_BRANCH "trunk"]
        [EGIT_SUBMODULES '()]
        [KEYWORDS '()]
        ))
 (display my-ebuild)
 ]

}
