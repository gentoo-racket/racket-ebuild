;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/templates/gh))


@(define gh-eval
   (make-base-eval '(require racket/class ebuild/ebuild ebuild/templates/gh)))


@title[#:tag "ebuild-templates-gh"]{Git Hosting snapshots}


@defmodule[ebuild/templates/gh]


@defmixin[ebuild-gh-mixin (ebuild%) ()]{}


@defclass[
 ebuild-gh% ebuild% ()
 ]{
 Pre-made class extending @racket[ebuild%] for writing ebuilds using the
 @link["https://gitlab.com/src_prepare/racket/racket-overlay/-/blob/master/eclass/gh.eclass"]{
  gh.eclass}.

 @defconstructor[
 (
  [GH_REPO    string?]
  [GH_DOM     string?  "gitlab.com"]
  [GH_COMMIT  (or/c #f string?) #f]
  )
 ]{
 }

 @examples[
 #:eval gh-eval
 (define my-ebuild
   (new ebuild-gh%
        [GH_DOM "gitlab.com"]
        [GH_REPO "asd/asd"]
        [GH_COMMIT "b46c957f0ad7490bc7b0f01da0e80380f34cac2d"]
        ))
 (display my-ebuild)
 ]

}
