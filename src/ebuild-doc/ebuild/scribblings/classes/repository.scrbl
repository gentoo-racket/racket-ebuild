;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     racket/class
                     ebuild))


@(define repository-eval
   (make-base-eval '(require racket/class ebuild)))


@title[#:tag "ebuild-classes-repository"]{Repository Class}


@defmodule[ebuild/repository]

@defclass[
 repository% object% ()
 ]{
 Repository class.

 For creating ebuild repository structures.
 @defconstructor[
 (
  [name      string?]
  [layout    layout? (default-layout)]
  [packages  (listof (is-a?/c package%))]
  )
 ]{
  By default if @racket[layout] is not given @racket[default-layout]
  (parameter variable) is used.
 }

 @defmethod[
 (show)
 void
 ]{
  Display repository @racket[name], @racket[layout] and
  @racket[packages] it contains.
 }

 @defmethod[
 (layout-string)
 string?
 ]{
  Wrapper for @racket[layout->string].
 }

 @defmethod[
 (save-layout
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates directory @filepath{metadata} with a file @filepath{layout.conf}
  in the given location (or current directory).
 }

 @defmethod[
 (save-name
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates directory @filepath{profiles} with a file @filepath{repo_name}
  in the given location (or current directory).
 }

 @defmethod[
 (save-packages
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates directory @filepath{CATEGORY/PN} with a @filepath{metadata.xml} file
  and @filepath{P.ebuild} ebuilds in the given location (or current directory).
 }

 @defmethod[
 (save
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates a full ebuild repository directory structure.
  (uses @method[repository% save-layout], @method[repository% save-name]
  and @method[repository% save-packages]).
 }
}

@examples[
 #:eval repository-eval
 (define r
   (new repository%
        [name "test"]
        [packages
         (list
          (new package%
               [CATEGORY "sys-devel"]
               [PN "asd"]
               [ebuilds (hash (simple-version "1.1.1") (new ebuild%))]
               [metadata
                (new metadata%
                     [upstream
                      (upstream
                       (list) #f #f #f (list (remote-id 'gitlab "asd/asd")))]
                     )]
               ))]
        ))
 (display (send r layout-string))
 (send r show)
 ]
