;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     xml
                     ebuild/metadata
                     ebuild/transformers/metadata))


@(define doc-eval
   (make-base-eval '(require ebuild/transformers/metadata)))


@title[#:tag "ebuild-transformers-metadata"]{Metadata Transformers}

@defmodule[ebuild/transformers/metadata]


@section{longdescription transformation}

@defproc[
 (xexpr->longdescriptions
  [xexpr  xexpr?]
  )
 (listof longdescription?)
 ]{
 Converts a @racket[xexpr] to @racket[list] of @racket[longdescription]s.

 @examples[
 #:eval doc-eval
 (xexpr->longdescriptions
  '(pkgmetadata (longdescription ((lang "en")) "Description")
                (longdescription ((lang "pl")) "Opis")))
 ]
}


@section{maintainer transformation}

@defproc[
 (xexpr->maintainers
  [xexpr  xexpr?]
  )
 (listof maintainer?)
 ]{
 Converts a @racket[xexpr] to @racket[list] of @racket[maintainer]s.

 @examples[
 #:eval doc-eval
 (xexpr->maintainers
  '(pkgmetadata (maintainer ((type "person"))
                            (email "asd@asd.asd") (name "A.S.D."))))
 ]
}


@section{slots transformation}

@defproc[
 (xexpr->slots
  [xexpr  xexpr?]
  )
 (listof slots?)
 ]{
 Converts a @racket[xexpr] to @racket[list] of @racket[slots].

 @examples[
 #:eval doc-eval
 (xexpr->slots '(pkgmetadata (slots (slot ((name "1.9"))
                                          "Provides libmypkg19.so.0"))))
 ]
}


@section{stabilize-allarches transformation}

@defproc[
 (xexpr->stabilize-allarches
  [xexpr  xexpr?]
  )
 boolean?
 ]{
 Converts a @racket[xexpr] to @racket[boolean].

 @examples[
 #:eval doc-eval
 (xexpr->stabilize-allarches '(pkgmetadata (stabilize-allarches ())))
 ]
}


@section{upstream transformation}

@defproc[
 (xexpr->upstream
  [xexpr  xexpr?]
  )
 upstream?
 ]{
 Converts a @racket[xexpr] to @racket[upstream].

 @examples[
 #:eval doc-eval
 (xexpr->upstream '(pkgmetadata (upstream
                                 (remote-id ((type "gitlab")) "asd/asd")
                                 (remote-id ((type "github")) "asd-org/asd"))))
 ]
}


@section{use transformation}

@defproc[
 (xexpr->uses
  [xexpr  xexpr?]
  )
 (listof use?)
 ]{
 Converts a @racket[xexpr] to @racket[list] of @racket[use]s.

 @examples[
 #:eval doc-eval
 (xexpr->uses '(pkgmetadata (use (flag ((name "racket"))
                                       "Build using dev-scheme/racket"))))
 ]
}


@section{metadata transformation}

@defproc[
 (xexpr->metadata
  [xexpr  xexpr?]
  )
 metadata%
 ]{
 Converts a @racket[xexpr] to @racket[metadata%] object.

 @examples[
 #:eval doc-eval
 (xexpr->metadata '(pkgmetadata))
 ]
}

@defproc[
 (read-metadata
  [port  input-port?  current-input-port]
  )
 metadata%
 ]{
 Converts input from @racket[port] to @racket[metadata%] object.

 @examples[
 #:eval doc-eval
 {define str #<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE pkgmetadata SYSTEM "https://www.gentoo.org/dtd/metadata.dtd">
<pkgmetadata>
</pkgmetadata>
EOF
   }
 (display (read-metadata (open-input-string str)))
 ]
}

@defproc[
 (read-metadata-file
  [pth  path-string?]
  )
 metadata%
 ]{
 Converts contents of @racket[file] from @racket[pth]
 to @racket[metadata%] object.
}
