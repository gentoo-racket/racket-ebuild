;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require
  (for-label racket
             ebuild
             ebuild/transformers/uncache))


@title[#:tag "ebuild-transformers-uncache"]{UnCache}

@defmodule[ebuild/transformers/uncache]


@defproc[
 (uncache
  [port  input-port?]
  )
 ebuild%
 ]{
 Consumes @racket[port] and attempts to convert
 it to a @racket[ebuild%] (racket object).

 Certain to be missed are any additional shell script functions
 because cached ebuild files contain only special ebuild variables.
}

@defproc[
 (uncache-file
  [pth  path-string?]
  )
 ebuild%
 ]{
 Attempts to convert a cached ebuild file at @racket[pth] to
 a @racket[ebuild%] (racket object).
}
