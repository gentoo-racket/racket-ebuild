;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     net/url
                     ebuild/transformers/githost))


@(define doc-eval
   (make-base-eval '(require net/url ebuild/transformers/githost)))


@title[#:tag "ebuild-transformers-githost"]{GitHost}

@defmodule[ebuild/transformers/githost]


@defstruct[
 githost
 ([domain      string?]
  [repository  string?])
 ]{
 @examples[
 #:eval doc-eval
 (githost "gitlab.com" "asd/asd")
 ]
}

@defproc[
 (url->githost [ur url?]) githost?
 ]{
 Converts a @racket[ur url] to @racket[githost].

 @examples[
 #:eval doc-eval
 (url->githost (string->url "https://gitlab.com/asd/asd"))
 ]
}

@defproc[
 (string->githost [str string?]) githost?
 ]{
 Converts a @racket[str string] to @racket[githost].

 @examples[
 #:eval doc-eval
 (string->githost "https://gitlab.com/asd/asd.git")
 ]
}

@defproc[
 (githost->string [gh githost?]) string?
 ]{
 Converts a @racket[gh  githost] to @racket[string].

 @examples[
 #:eval doc-eval
 (githost->string (githost "gitlab.com" "asd/asd"))
 ]
}

@defproc[
 (githost->url [gh githost?]) url?
 ]{
 Converts a @racket[gh  githost] to @racket[url].

 @examples[
 #:eval doc-eval
 (githost->url (githost "gitlab.com" "asd/asd"))
 ]
}
