#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/class
         racket/cmdline
         ebuild/version
         "private/url2pkg.rkt")


(define (main)
  (define pn (make-parameter "package"))
  (define pv (make-parameter "0.0.0"))
  (define action (make-parameter 'show))

  (define program "url2pkg")

  (command-line
   #:program program

   #:ps
   ""
   "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
   "Licensed under the GNU GPL v2 License"

   #:once-each
   [("--pn" "--package-name")
    package-name
    "Package name"
    (pn package-name)]
   [("--pv" "--package-version")
    package-version
    "Package version"
    (pv package-version)]
   [("-V" "--version")
    "Show the version of this program"
    (print-version program)
    (exit 0)]

   #:once-any
   [("--save")
    "Save generated package"
    (action 'save)]
   [("--show")
    "Show generated package"
    (action 'show)]

   #:args (url-string)
   (let ([pkg (url->pkg url-string (pn) (pv))])
     (case (action)
       [(save)
        (send pkg save)]
       [(show)
        (send pkg show)]))))


(module+ main
  (main))
