#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/cmdline
         threading
         ebuild/version
         "private/manifest.rkt")


(define (main)
  (define program
    "manifest")

  (define force?
    (make-parameter #false))

  (define jobs
    (make-parameter
     4
     (lambda (n)
       (cond
         [(<= n 0)
          (error 'jobs "Expected a exact non-negative integer, given ~v" n)]
         [else
          n]))))

  (command-line
   #:program program

   #:ps
   ""
   "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
   "Licensed under the GNU GPL v2 License"

   #:once-each
   [("-f" "--force")
    "Force regeneration of distfiles"
    (force? #true)]
   [("-j" "--jobs")
    number
    "Number of parallel jobs to run"
    (jobs (string->number number))]
   [("-V" "--version")
    "Show the version of this program"
    (print-version program)
    (exit 0)]

   #:args directory-paths
   (let ([directory-complete-paths
          (~>> directory-paths
               (map string->path)
               (map path->complete-path))])
     (for ([p directory-complete-paths])
       (manifest-directory p #:force? (force?) #:jobs (jobs))))))


(module+ main
  (main))
