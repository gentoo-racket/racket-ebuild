#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/cmdline
         (only-in threading ~>> lambda~>>)
         (prefix-in file:   (only-in racket/file file->string))
         (prefix-in system: (only-in racket/system system))
         (prefix-in git:    ziptie/git/git)
         (prefix-in git:    ziptie/git/staged)
         (prefix-in upi:    upi/basename)
         (prefix-in ebuild: (only-in ebuild/package package-version-string?))
         (prefix-in ebuild: ebuild/break-string)
         (prefix-in ebuild: ebuild/version)
         (prefix-in commit: "private/commit.rkt")
         (prefix-in keywords: "private/keywords.rkt")
         (prefix-in manifest: "private/manifest.rkt")
         (prefix-in pkgname: "private/pkgname.rkt")
         (prefix-in fix-head: (only-in "private/fix-head.rkt" fix-head/file)))


(define program
  "commit")

(define dry-run?
  (make-parameter #false))

(define stage
  (make-parameter 'none))

(define force-edit?
  (make-parameter #false))

(define editor
  (make-parameter (or (getenv "EDITOR") "vi")))

(define aux-message
  (make-parameter #false ebuild:break-string))

(define message
  (make-parameter ""
                  (lambda (msg)
                    (when (equal? msg "")
                      (eprintf "[WARNING] empty message!~%"))
                    msg)))

(define signoff?
  (make-parameter #false))

(define manifests?
  (make-parameter #false))

(define test-build?
  (make-parameter #false))

(define scan?
  (make-parameter #false))

(define scan-fatal?
  (make-parameter #true))

(define scan-ask-after-failure?
  (make-parameter #false))

(define bug-list
  (make-parameter '()))

(define close-list
  (make-parameter '()))

;; Special variable for usage with "magic-*".
(define magic-package-version
  (make-parameter #false
                  (lambda (package-version)
                    (unless (ebuild:package-version-string? package-version)
                      (error 'package-version
                             "not a valid package-version string, given ~v"
                             package-version))
                    package-version)))

(define magic-bump
  (make-parameter #false))

(define magic-drop
  (make-parameter #false))


(define (main)
  (command-line
   #:program program

   #:ps
   ""
   "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
   "Licensed under the GNU GPL v2 License"

   #:once-any
   [("-a" "--all")
    "Stage all changed/new/removed files"
    (stage 'all)]
   [("-u" "--update")
    "Stage all changed files"
    (stage 'update)]

   #:once-each
   [("-d" "--dry-run")
    "Do not make commits"
    (dry-run? #true)]
   [("-e" "--edit")
    "Force editing the commit even if message is not empty"
    (force-edit? #true)]
   [("-E" "--editor")
    executable
    "Overwrite the EDITOR environment variable"
    (editor executable)]

   [("-m" "--message")
    commit-message
    "Specify the commit message"
    (message commit-message)]
   [("-M" "--aux-message")
    commit-message
    "Auxiliary commit message, auto-line-wrapped"
    (aux-message commit-message)]

   [("-T" "--test-build")
    "Test modified, staged ebuilds by executing their test phases"
    (test-build? #true)]
   [("-s" "--scan")
    "Scan using \"pkgcheck\""
    (scan? #true)]
   [("-n" "--scan-nonfatal")
    "Do not fail when scanning with \"pkgcheck\""
    (scan-fatal? #false)]
   [("-k" "--ask-continue")
    "Ask whether user wants to continue after a failed \"pkgcheck\" scan"
    (scan-ask-after-failure? #true)]

   [("-B" "--bump")
    package-version
    "Set the commit message to \"bump to PACKAGE-VERSION\" (updating ebuilds)"
    (magic-package-version package-version)
    (message (string-append "bump to " package-version))]
   [("--bump-copy")
    "If doing a package bump, copy the latest ebuild as that version"
    (magic-bump #true)]

   [("-D" "--drop")
    package-version
    "Set the commit message to \"drop old PACKAGE-VERSION\" (removing ebuilds)"
    (magic-package-version package-version)
    (message (string-append "drop old " package-version))]
   [("--drop-remove")
    "If doing a package drop, remove the specified ebuild version"
    (magic-drop #true)]

   [("-U" "--update-manifests")
    "Update Manifest files"
    (manifests? #true)]

   [("-S" "--sign")
    "Sign the created commit"
    (signoff? #true)]

   [("-V" "--version")
    "Show the version of this program"
    (ebuild:print-version program)
    (exit 0)]

   #:multi
   [("-b" "--bug")
    gentoo-bug/url
    "Add \"Bug\" tag for a given Gentoo or upstream bug/URL"
    (bug-list `(,@(bug-list) ,gentoo-bug/url))]
   [("-c" "--closes")
    gentoo-bug/url
    "Add \"Closes\" tag for a given Gentoo or upstream bug/URL"
    (close-list `(,@(close-list) ,gentoo-bug/url))]

   #:args ()
   ;; We do not use toplevel yet but want to check it anyway
   ;; to see if we are inside a git repository.
   (git:git "rev-parse --show-toplevel" #:fallback? #false)

   ;; Magic switches

   (when (magic-bump)
     (let* ([package-short-name
             ;; CONSIDER: Could this break? Do we need a specific error if so?
             (~>> (current-directory)
                  pkgname:path->package
                  upi:basename)]
            [package-highest
             (~>> (directory-list)
                  (filter (lambda (pth)
                            (~>> package-short-name
                                 (string-append _ ".*.ebuild")
                                 regexp
                                 (regexp-match-exact? _ pth))))
                  ;; Because live version might not support regular releases.
                  (filter (lambda~>> (regexp-match-exact? ".*9999.*") not))
                  (map path->string)
                  (sort _ string>?)
                  car)]
            [package-target
             (format "~a-~a.ebuild"
                     package-short-name
                     (magic-package-version))])
       (cond
         [(dry-run?)
          (eprintf
           "[INFO] Would copy \"~a\" to \"~a\" (but dry-run is enabled).~%"
           package-highest
           package-target)]
         [else
          (printf "[INFO] Copying \"~a\" to \"~a\".~%"
                  package-highest
                  package-target)
          (copy-file package-highest package-target)
          (keywords:all-keywords-to-unstable package-target #true)])))

   (when (magic-drop)
     (let ([file-path
            (~>> (current-directory)
                 pkgname:path->package
                 upi:basename
                 (format "~a-~a.ebuild" _ (magic-package-version))
                 (build-path (current-directory)))])
       (cond
         [(dry-run?)
          (eprintf "[INFO] Would delete \"~a\" (but dry-run is enabled).~%"
                   file-path)]
         [else
          (printf "[INFO] Deleting \"~a\".~%" file-path)
          (delete-file file-path)])))

   ;; Update manifests

   (cond
     [(and (dry-run?) (manifests?))
      (displayln "[WARNING] Dry run enabled, not updating manifests!")]
     [(not (manifests?))
      (void)]
     [else
      (manifest:manifest-directory (current-directory))])

   ;; Check for errors

   (cond
     [(not (scan?))
      (void)]
     [(not (system:system
            "pkgcheck scan --exit error,warning,style --verbose ."))
      ;; ^ added "not", so true if command fails
      => (lambda (failure)
           (cond
             [(and failure (scan-fatal?))
              (error 'pkgcheck "scan failed, errors fatalized, exiting!")]
             [(and failure (scan-ask-after-failure?))
              (display "Scan using \"pkgcheck\" failed. Continue? [Y/n] ")
              (let ([answer (read-line)])
                (cond
                  [(regexp-match-exact? #rx"((Y|y)((E|e)(S|s)|)|)" answer)
                   (printf "Continuing. (answer was: ~v)~%" answer)]
                  [else
                   (error 'pkgcheck
                          "Exiting with error! (answer was: ~v)"
                          answer)]))]
             [else
              (displayln
               "[WARNING] pkgcheck scan failed but errors nonfatalized.")]))]
     [else
      (displayln "[INFO] pkgcheck scan passed, continuing...")])

   ;; Git: add files

   (if (dry-run?)
       (displayln "[WARNING] Dry run enabled, not staging anything!")
       (case (stage)
         [(none)   (void)]
         [(all)    (git:git "add --all .")]
         [(update) (git:git "add --update .")]))

   (define staged-ebuilds
     (for/list ([f (git:git-get-staged #:relative? #true)]
                #:when (and (file-exists? f)
                          (regexp-match-exact? #rx".*\\.ebuild" f)))
       f))

   ;; Fix ebuild headers

   (for ([f staged-ebuilds])
     (fix-head:fix-head/file f)
     (git:git (format "add ~a" f)))

   ;; Compile test
   (let ([test-build-format
          "FEATURES=\"test\" USE=\"test\" ebuild ~a clean test clean"])
     (when (test-build?)
       (for ([f staged-ebuilds]
             #:when (not (system:system (format test-build-format f))))
         (error 'test-build "Running \"src_test\" of ebuild ~a failed!" f))))

   ;; Git: commit

   (let* ([commit-file
           (commit:make-commit-file (message)
                                    (aux-message)
                                    (bug-list)
                                    (close-list))])
     (when (or (equal? (message) "") (force-edit?))
       (system:system (format "~a ~a" (editor) (path->string commit-file))))
     (cond
       [(dry-run?)
        (displayln "[INFO] Following commit would have been made:")
        (display (file:file->string commit-file))]
       [else
        (commit:commit-from-file commit-file (signoff?))])
     ;; Cleanup
     (delete-file commit-file)
     (when (null? (directory-list commit:temp-dir))
       (delete-directory commit:temp-dir)))))


(module+ main
  (main))
