;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/class
         ebuild/metadata
         ebuild/transformers/githost)

(provide githost->metadata)


;; CONSIDER: extract longdescription from the page
;; CONSIDER: maybe we need a web crawler or stuff like that?
;; for network: use-network?

(define (githost->metadata dom repo verbose?)
  (let* ([gh  (githost dom repo)]
         [base
          (let ([s  (githost->string gh)])
            (if (regexp-match-exact? #rx".*/$" s) s (string-append s "/")))]
         [remo
          (cond
            [(regexp-match-exact? #rx".*github.com.*" dom)  'github]
            [(regexp-match-exact? #rx".*gitlab.com.*" dom)  'gitlab]
            [else
             (when verbose?
               (printf "[WARNING] Domain \"~a\" not recognized~%" dom))
             #f])]
         [chlog
          (case remo
            [(github)  (format "~areleases/"   base)]
            [(gitlab)  (format "~a-/releases/" base)]
            [else  #f])]
         [bugs
          (case remo
            [(github)  (format "~aissues/"   base)]
            [(gitlab)  (format "~a-/issues/" base)]
            [else  #f])]
         [maybe-upstream
          (upstream '()    ; maintainers
                    chlog  ; changelog
                    #f     ; doc
                    bugs   ; bugs-to
                    (if remo (list (remote-id remo repo)) '())  ; remote-ids
                    )])
    (new metadata% [upstream (and remo maybe-upstream)])))
