;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in racket/string string-join)
         (only-in threading ~>> lambda~>>))

(provide (all-defined-out))


(define (file->keywords-line [file-path : Path-String]) : (Option String)
  (for/or ([line (in-port read-line (open-input-file file-path))]
           #:when (regexp-match-exact? #px"\\s*KEYWORDS=.+" line))
    line))

(define (string->keywords [str : String]) : (Listof String)
  (~>> str
       (regexp-replace #rx"#.*" _ "")
       (regexp-match #rx"(?<=KEYWORDS=).+")
       (or _ '(""))
       car
       (regexp-replace* "\"" _ "")
       (regexp-split #px"\\s+")
       (filter (lambda~>> (equal? "") not))))

(define (->unstable [keyword : String]) : String
  (cond
    ;; Keyword is either special "-*"
    ;;   or is already unstable.
    [(or (equal? keyword "-*")
         (regexp-match-exact? #rx"~.+" keyword))
     keyword]
    ;; Keyword is explicitly unset.
    [(regexp-match-exact? #rx"-.+" keyword)
     (->unstable (substring keyword 1))]
    [else
     (string-append "~" keyword)]))

(define (keywords-add [keywords : (Listof String)]
                      [keywords-add-list : (Listof String)])
        : (Listof String)
  (for/fold ([result-keywords keywords])
            ([add-keyword keywords-add-list])
    (cond
      ;; Keyword is already in result-keywords.
      [(member add-keyword result-keywords)
       result-keywords]
      ;; Replace unstable with stable.
      [(member (->unstable add-keyword) result-keywords)
       (~>> result-keywords
            (remove (->unstable add-keyword))
            (cons add-keyword))]
      ;; Add to result-keywords.
      [else
       (cons add-keyword result-keywords)])))

(define (keywords-remove [keywords : (Listof String)]
                         [keywords-remove-list : (Listof String)])
        : (Listof String)
  (remove* keywords-remove-list keywords))

(define (keyword-strip [keyword : String]) : String
  (if (member (string-ref keyword 0) '(#\- #\~))
      (substring keyword 1)
      keyword))

(define (explicitly-unset-keyword? [keyword : String]) : Boolean
  (equal? #\- (string-ref keyword 0)))

(define (unstable-keyword? [keyword : String]) : Boolean
  (equal? #\~ (string-ref keyword 0)))

(define (stable-keyword? [keyword : String]) : Boolean
  (not (or (explicitly-unset-keyword? keyword) (unstable-keyword? keyword))))

(define (compound-keyword? [keyword : String]) : Boolean
  (regexp-match-exact? #rx".+-.+" keyword))

(define (all-keyword? [keyword : String]) : Boolean
  (regexp-match-exact? #rx"^[-|~]?\\*$" keyword))

;; keywords-sort is meant for sort (as the second argument).

(define (keywords-sort [keyword-A : String] [keyword-B : String]) : Boolean
  (cond
    ;; Special "-*", "*", "~*" keywords,
    ;;   in that order, see:
    ;;   https://gitweb.gentoo.org/proj/gentoolkit.git/tree/pym/gentoolkit/ekeyword/ekeyword.py#n97
    [(and (all-keyword? keyword-A) (all-keyword? keyword-B))
     (cond
       [(explicitly-unset-keyword? keyword-A) #true]
       [(explicitly-unset-keyword? keyword-B) #false]
       [(stable-keyword? keyword-A) #true]
       [(stable-keyword? keyword-B) #false]
       [(unstable-keyword? keyword-A) #true]
       [else
        #false])]
    [(all-keyword? keyword-A)
     #true]
    [(all-keyword? keyword-B)
     #false]
    ;; "x86-linux" "x86" => wrong
    [(and (compound-keyword? keyword-A) (not (compound-keyword? keyword-B)))
     #false]
    ;; "x86" "x86-linux" => right
    [(and (not (compound-keyword? keyword-A)) (compound-keyword? keyword-B))
     #true]
    [else
     (string<=? (keyword-strip keyword-A) (keyword-strip keyword-B))]))

(define (replace-string-keywords [str : String]
                                 [keywords : (Listof String)])
        : String
  (~>> keywords
       string-join
       (string-append "\"" _ "\"")
       (regexp-replace #rx"(?<=KEYWORDS=).+" str)))

;; First removes and then adds the KEYWORDS.

(define (replace-ebuild-keywords [file-path : Path-String]
                                 [keywords-add-list : (Listof String)]
                                 [keywords-remove-list : (Listof String)]
                                 [verbose? : Boolean #false])
        : Void
  (let ([keywords-line
         (file->keywords-line file-path)]
        [output-buffer
         (open-output-string)])
    (when keywords-line
      (when verbose?
        (printf "[INFO] Found KEYWORDS line: ~v~%" keywords-line))
      (let ([new-keywords
             (~>> keywords-line
                  string->keywords
                  (keywords-remove _ keywords-remove-list)
                  (keywords-add _ keywords-add-list)
                  (sort _ keywords-sort))])
        (when verbose?
          (printf "[INFO] New keywords: ~a~%" new-keywords))
        (for ([line (in-port read-line (open-input-file file-path))])
          (cond
            [(regexp-match-exact? #px"\\s*KEYWORDS=.+" line)
             (displayln (replace-string-keywords line new-keywords)
                        output-buffer)]
            [else
             (displayln line output-buffer)])))
      (with-output-to-file file-path #:exists 'replace
        (lambda ()
          (display (get-output-string output-buffer)))))))

(define (all-keywords-to-unstable [file-path : Path-String]
                                  [verbose? : Boolean #false])
        : Void
  (let ([keywords-line
         (file->keywords-line file-path)])
    (when keywords-line
      (let* ([old-keywords
              (string->keywords keywords-line)]
             [new-keywords
              (for/list ([k : String (in-list old-keywords)])
                : (Listof String)
                (if (regexp-match #rx"^~" k)
                    k
                    (->unstable k)))])
        (replace-ebuild-keywords file-path
                                 new-keywords
                                 old-keywords
                                 verbose?)))))
