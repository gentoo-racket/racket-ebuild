;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/list take)
         upi/ls)

(provide (all-defined-out))


;; return all directories in a given directory
;; Path-String? -> (Listof (Pairof String String))
(define (dirs pth)
  (for/list ([(k v) (in-hash (ls pth))]
             #:when (equal? (hash-ref v 'type) 'directory)
             #:when (not (member k '("." ".."))))
    (cons k (hash-ref v 'path))))

;; ebuild regex
(define ebrx #rx"^.+-.+.ebuild$")

(define (find-ebuilds [pth "."])
  (filter (lambda (f) (regexp-match-exact? ebrx f))
          (map path->string (directory-list pth #:build? #t))))

;; (Listof (Pairof Any Any)) List (U exclude include) -> (Listof Pair)
(define (filterl origin excludes/includes [exclude/include 'exclude])
  (define exclude/include-proc
    (case exclude/include
      [(exclude) not]
      [(include) (lambda (v) v)]
      [else (error "wrong arg")]))
  (cond
    [(null? origin) '()]
    [(null? excludes/includes) origin]
    [else
     (filterl (filter (lambda (o)
                        (exclude/include-proc
                         (regexp-match? (car excludes/includes) (car o))))
                      origin)
              (cdr excludes/includes))]))

(define (exclude/only origin excludes onlys)
  (filterl (filterl origin excludes 'exclude) onlys 'include))

(define (search-old maxn
                    [pth "."]
                    #:excl-categ [excl-categ '()]
                    #:only-categ [only-categ '()]
                    #:excl-names [excl-names '()]
                    #:only-names [only-names '()])
  (define old '())
  ;; category directory
  (for ([cat (map cdr (exclude/only (dirs pth) excl-categ only-categ))])
    ;; package directory
    (for ([pkg (map cdr (exclude/only (dirs cat) excl-names only-names))])
      (let* ([col (find-ebuilds pkg)] [len (length col)])
        (when (> len maxn)
          (set! old (append old (take (sort col string<?) (- len maxn))))))))
  (sort old string<?))

(define (check-repository-structure pth)
  (define repo_name-path (build-path pth "profiles/repo_name"))
  (when (not (file-exists? repo_name-path))
    (error 'ERROR
           "Bad repository structure. File ~a does not exist."
           repo_name-path)))

(define (clean-old maxn
                   pth
                   simulate?
                   verbose?
                   excl-categ
                   only-categ
                   excl-names
                   only-names)
  (check-repository-structure pth)
  (for ([old (search-old maxn
                         pth
                         #:excl-categ excl-categ
                         #:only-categ only-categ
                         #:excl-names excl-names
                         #:only-names only-names)])
    (when verbose?
      (displayln (string-append "Cleaning: " old)))
    (when (not simulate?)
      (delete-file old))))
