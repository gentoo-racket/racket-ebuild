#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/cmdline
         ebuild/version
         "private/keywords.rkt")


(define (main)
  (let ([program
         "keywords"]
        [verbose?
         (make-parameter #false)]
        [keywords-add-list
         (make-parameter '())]
        [keywords-remove-list
         (make-parameter '())])
    (command-line
     #:program program
     #:ps
     ""
     "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
     "Licensed under the GNU GPL v2 License"
     #:multi
     [("-a" "--add")
      keyword
      "Add a architecture keyword"
      (keywords-add-list (cons keyword (keywords-add-list)))]
     [("-r" "--remove")
      keyword
      "Remove a architecture keyword"
      (keywords-remove-list (cons keyword (keywords-remove-list)))]
     #:once-each
     [("-v" "--verbose")
      "Be verbose (detailed console output)"
      (verbose? #t)]
     [("-q" "--quiet")
      "Be quiet (minimal/no console output)"
      (verbose? #f)]
     [("-V" "--version")
      "Show the version of this program"
      (print-version program)
      (exit 0)]
     #:args file-paths
     (let ()
       (when (and (verbose?) (null? file-paths))
         (displayln "[WARNING] No files given."))
       (for ([file-path file-paths])
         (replace-ebuild-keywords file-path
                                  (keywords-add-list)
                                  (keywords-remove-list)
                                  (verbose?)))))))


(module+ main
  (main))
