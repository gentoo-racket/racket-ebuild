#lang info


(define pkg-desc "Library to ease ebuild creation. Modification DSL.")

(define version "16.1.1")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "ebuild-lib"))

(define build-deps
  '())
