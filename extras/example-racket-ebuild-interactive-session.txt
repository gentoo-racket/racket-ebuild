Input Package category ... sci-mathematics
Input Package name ... eprover
Input Package release ... 2.6
Input Package phase? ...
Input Package prerelease? ...
Input Package release candidate? ...
Input Package patch? ...
Input Package revision? ...
Inherits? [y/N] ...
Input Package description ... Theorem prover for full first-order logic with equality
Input Upstream website ... https://wwwlehre.dhbw-stuttgart.de/~sschulz/E/E.html
Source URL? [y/N] ... y
Input Source URL ... https://github.com/eprover/eprover/archive/refs/tags/E-2.6.tar.gz
Rename source? [y/N] ... y
Input -> ... E-${PV}.tar.gz
Custom ${S} ? [y/N] ...
Input Package license ... GPL-2
USE flags? [y/N] ...
Runtime dependencies? [y/N] ...
------------------------------
sci-mathematics/eprover
2.6
# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Theorem prover for full first-order logic with equality"
HOMEPAGE="https://wwwlehre.dhbw-stuttgart.de/~sschulz/E/E.html"
SRC_URI="https://github.com/eprover/eprover/archive/refs/tags/E-2.6.tar.gz -> E-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE pkgmetadata SYSTEM "https://www.gentoo.org/dtd/metadata.dtd">

<pkgmetadata>
</pkgmetadata>

Save? [y/N] ... y
