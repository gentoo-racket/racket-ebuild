#compdef racket-ebuild-commit
#autoload

local global_opts=(
    '(--bump-copy)'--bump-copy'[If doing a package bump, copy the latest ebuild as that version]'
    '(--drop-remove)'--drop-remove'[If doing a package drop, remove the specified ebuild version]'
    '(--help -h)'{--help,-h}'[Show help information with usage options and exit]'
    '(-B --bump)'{-B,--bump}'[Set the commit message to "bump to PACKAGE-VERSION" (updating ebuilds)]:package-version:'
    '(-D --drop)'{-D,--drop}'[Set the commit message to "drop old PACKAGE-VERSION" (removing ebuilds)]:package-version:'
    '(-E --editor)'{-E,--editor}'[Overwrite the EDITOR environment variable]:executable:'
    '(-M --aux-message)'{-M,--aux-message}'[Auxiliary commit message, auto-line-wrapped]:commit-message:'
    '(-S --sign)'{-S,--sign}'[Sign the created commit]'
    '(-T --test-build)'{-T,--test-build}'[Test modified, staged ebuilds by executing their test phases]'
    '(-U --update-manifests)'{-U,--update-manifests}'[Update Manifest files]'
    '(-V --version)'{-V,--version}'[Show the version of this program]'
    '(-a --all)'{-a,--all}'[Stage all changed/new/removed files]'
    '(-d --dry-run)'{-d,--dry-run}'[Do not make commits]'
    '(-e --edit)'{-e,--edit}'[Force editing the commit even if message is not empty]'
    '(-k --ask-continue)'{-k,--ask-continue}'[Ask whether user wants to continue after a failed "pkgcheck" scan]'
    '(-m --message)'{-m,--message}'[Specify the commit message]:commit-message:'
    '(-n --scan-nonfatal)'{-n,--scan-nonfatal}'[Do not fail when scanning with "pkgcheck"]'
    '(-s --scan)'{-s,--scan}'[Scan using "pkgcheck"]'
    '(-u --update)'{-u,--update}'[Stage all changed files]'
    '*'{-b,--bug}'[Add "Bug" tag for a given Gentoo or upstream bug/URL]:gentoo-bug/url:'
    '*'{-c,--closes}'[Add "Closes" tag for a given Gentoo or upstream bug/URL]:gentoo-bug/url:'
)

_arguments ${global_opts[@]}
